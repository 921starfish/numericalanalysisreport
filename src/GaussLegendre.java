import java.util.function.DoubleUnaryOperator;

public class GaussLegendre {
	static double pn1 = 0.0;

	public static void main(String[] args) {
		double a = 0;// 積分範囲
		double b = 1;// 積分範囲
		int n = 1000;// n次の指定
		DoubleUnaryOperator f = x -> Math.exp(-(x * x));// 被積分関数

		System.out.println("Gauss-Legendreの" + n + "点積分");
		double S = gaussLegendreN(f, a, b, n);
		System.out.println("∫[Math.exp(-(x * x)), " + a + ", " + b + "] = " + S);
	}

	// Gauss-Legendreのn点積分
	public static double gaussLegendreN(DoubleUnaryOperator f, double a, double b, int n) {
		// xとαの導出
		double[] x = new double[n];
		double[] alpha = new double[n];

		for (int k = n; k >= 1; k--) {
			double a0 = -1.0;
			double b0 = 1.0;
			double c = 0.0;
			for (int i = 0; i < 70; i++) {
				c = (a0 + b0) * 0.5;
				int NN = sign(n, c);
				if (NN >= k) {
					a0 = c;
				} else {
					b0 = c;
				}
			}
			double w = 2.0 * (1.0 - c * c) / pn1 / pn1 / (double) n / (double) n;
			x[k - 1] = c;
			alpha[k - 1] = w;
		}
		// xとαの確認部
		System.out.print("double[] x = {");
		for (int i = 0; i < n; i++) {
			System.out.print(x[i] + ", ");
		}
		System.out.println("};");
		System.out.print("double[] α = {");
		for (int i = 0; i < n; i++) {
			System.out.print(alpha[i] + ", ");
		}
		System.out.println("};");
		// 確認部終了

		double S = 0;

		for (int i = 0; i < n; i++) {
			S += alpha[i] * f.applyAsDouble(x[i] * (b - a) / 2 + (a + b) / 2);
		}
		S *= (b - a) / 2;
		return S;
	}

	public static int sign(int n, double t) {
		double p0 = 1.0;
		double p1 = t;
		int N = 0;
		double ifl = 1.0;
		if (p1 * ifl < 0) {
			N++;
			ifl = -ifl;
		}

		for (int i = 2; i <= n; i++) {
			double w = p1;
			p1 = (t * p1 * (double) (i + i - 1) - p0 * (double) (i - 1)) / (double) i;
			p0 = w;
			if (p1 * ifl < 0) {
				N++;
				ifl = -ifl;
			}
		}
		pn1 = p0;
		return N;
	}
}
