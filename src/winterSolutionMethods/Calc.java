package winterSolutionMethods;

public class Calc {

	// 行列・ベクトルのコンソール出力
	public static void printVec(double x[]) {
		System.out.print("( ");
		for (int i = 0; i < x.length; i++) {
			System.out.print(x[i] + " ");
		}
		System.out.println(")");
		return;
	}

	public static void printMat(double A[][]) {
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[i].length; j++) {
				System.out.print(A[i][j] + " ");
				if (j == A[i].length - 1) {
					System.out.println();
				}
			}
		}
		return;

	}

	// ベクトルに関する演算
	public static double[] scalarMultiple(double c, double x[]) {
		for (int i = 0; i < x.length; i++) {
			x[i] = c * x[i];
		}
		return x;
	}

	public static double[] addVec(double x[], double y[]) {
		if (x.length != y.length) {
			System.out.print("Calc.addVec:長さの違うベクトルの加算");
			return null;
		}
		double[] result = new double[x.length];
		for (int i = 0; i < x.length; i++) {
			result[i] = x[i] + y[i];
		}
		return result;
	}

	public static double[] subVec(double x[], double y[]) {
		if (x.length != y.length) {
			System.out.print("Calc.subVec:長さの違うベクトルの減算");
			return null;
		}
		double[] result = new double[x.length];
		for (int i = 0; i < x.length; i++) {
			result[i] = x[i] - y[i];
		}
		return result;
	}

	public static double innProd(double x[], double y[]) {
		if (x.length != y.length) {
			System.out.print("Calc.innProd:長さの違うベクトルの内積");
			return 0;
		}
		double result = 0;
		for (int i = 0; i < x.length; i++) {
			result += x[i] * y[i];
		}
		return result;
	}

	// 行列・ベクトルに関する演算
	public static double[] matVec(double A[][], double x[]) {
		if (A[0].length != x.length) {
			System.out.print("Calc.matVec:計算出来ない");
			return null;
		}

		double[] result = new double[A.length];
		for (int i = 0; i < result.length; i++) {
			result[i] = 0;
		}
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				result[i] += A[i][j] * x[j];
			}
		}
		return result;
	}

	public static double[] residual(double A[][], double x[], double[] b) {
		double[] Ax = matVec(A, x);
		double[] result = subVec(Ax, b);
		return result;
	}

	// 行列同士の演算
	public static double[][] addMat(double A[][], double B[][]) {
		// 例外処理未実装
		double[][] result = new double[A.length][A[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[i].length; j++) {
				result[i][j] = A[i][j] + B[i][j];
			}
		}
		return result;
	}

	public static double[][] multipleMat(double A[][], double B[][]) {
		if (A[0].length != B.length) {
			System.out.print("Calc.multipleMat:計算出来ない");
			return null;
		}
		double[][] result = new double[A.length][B[0].length];

		for (int k = 0; k < A.length; k++) {
			for (int i = 0; i < A.length; i++) {
				for (int j = 0; j < B[0].length; j++) {
					result[i][k] += A[i][j] * B[j][k];
				}
			}
		}
		return result;
	}

	// ベクトルノルム
	public static double vecNorm1(double x[]) {
		double result = 0;
		for (int i = 0; i < x.length; i++) {
			result += Math.abs(x[i]);
		}
		return result;
	}

	public static double vecNorm2(double x[]) {
		double temp = 0;
		for (int i = 0; i < x.length; i++) {
			temp += x[i] * x[i];
		}
		return Math.sqrt(temp);
	}

	public static double vecNormInf(double x[]) {
		double result = 0;
		for (int i = 0; i < x.length; i++) {
			result = Math.max(result, Math.abs(x[i]));
		}
		return result;
	}

	// 行列ノルム
	public static double matNorm1(double A[][]) {
		double temp[] = new double[A[0].length];
		for (int j = 0; j < A[0].length; j++) {
			for (int i = 0; i < A.length; i++) {
				temp[j] += Math.abs(A[i][j]);
			}
		}
		return vecNormInf(temp);

	}

	public static double matNormInf(double A[][]) {
		double temp[] = new double[A.length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				temp[i] += Math.abs(A[i][j]);
			}
		}

		return vecNormInf(temp);

	}

	public static double matNormFrobenius(double A[][]) {
		double sum = 0;
		for (int j = 0; j < A[0].length; j++) {
			for (int i = 0; i < A.length; i++) {
				sum += A[i][j] * A[i][j];
			}
		}

		return Math.sqrt(sum);

	}

}
