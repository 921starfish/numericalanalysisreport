package winterSolutionMethods;

import org.apache.commons.math3.complex.Complex;

public class ComplexCalc {

	// 行列・ベクトルのコンソール出力
	public static void printVec(Complex x[]) {
		System.out.print("( ");
		for (int i = 0; i < x.length; i++) {
			System.out.print(x[i] + " ");
		}
		System.out.println(")");
		return;
	}

	public static void printMat(Complex A[][]) {
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[i].length; j++) {
				System.out.print(A[i][j] + " ");
				if (j == A[i].length - 1) {
					System.out.println();
				}
			}
		}
		return;
	}

	// ベクトルに関する演算
	public static Complex[] scalarMultiple(Complex c, Complex x[]) {
		for (int i = 0; i < x.length; i++) {
			x[i] = c.multiply(x[i]);
		}
		return x;
	}

	public static Complex[] addVec(Complex x[], Complex y[]) {
		if (x.length != y.length) {
			System.out.print("Calc.addVec:長さの違うベクトルの加算");
			return null;
		}
		
		Complex[] result = new Complex[x.length];
		for (int i = 0; i < result.length; i++) {
			result[i] = new Complex(0, 0);
		}
		
		for (int i = 0; i < x.length; i++) {
			result[i] = x[i].add(y[i]);
		}
		return result;
	}

	public static Complex[] subVec(Complex x[], Complex y[]) {
		if (x.length != y.length) {
			System.out.print("Calc.subVec:長さの違うベクトルの減算");
			return null;
		}
		
		Complex[] result = new Complex[x.length];
		for (int i = 0; i < result.length; i++) {
			result[i] = new Complex(0, 0);
		}
		
		for (int i = 0; i < x.length; i++) {
			result[i] = x[i].subtract(y[i]);
		}
		return result;
	}

	// 行列・ベクトルに関する演算
	public static Complex[] matVec(Complex A[][], Complex x[]) {
		if (A[0].length != x.length) {
			System.out.print("Calc.matVec:計算出来ない");
			return null;
		}

		Complex[] result = new Complex[A.length];
		for (int i = 0; i < result.length; i++) {
			result[i] = new Complex(0, 0);
		}
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				result[i] = result[i].add(A[i][j].multiply(x[j]));
			}
		}
		return result;
	}

	public static Complex[] residual(Complex A[][], Complex x[], Complex[] b) {
		Complex[] Ax = matVec(A, x);
		Complex[] result = subVec(Ax, b);
		return result;
	}

	// ベクトルノルム
	public static double vecNorm1(Complex x[]) {
		double result = 0;
		for (int i = 0; i < x.length; i++) {
			result += x[i].abs();
		}
		return result;
	}

	public static double vecNorm2(Complex x[]) {
		double temp = 0;
		for (int i = 0; i < x.length; i++) {
			temp += x[i].abs() * x[i].abs();
		}
		return Math.sqrt(temp);
	}

	public static double vecNormInf(Complex x[]) {
		double result = 0;
		for (int i = 0; i < x.length; i++) {
			result = Math.max(result, x[i].abs());
		}
		return result;
	}

	// 行列ノルム
	public static double matNorm1(Complex A[][]) {
		double temp[] = new double[A[0].length];
		for (int j = 0; j < A[0].length; j++) {
			for (int i = 0; i < A.length; i++) {
				temp[j] += A[i][j].abs();
			}
		}
		return Calc.vecNormInf(temp);
	}

	public static double matNormInf(Complex A[][]) {
		double temp[] = new double[A.length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				temp[i] += A[i][j].abs();
			}
		}
		return Calc.vecNormInf(temp);
	}

	public static double matNormFrobenius(Complex A[][]) {
		double sum = 0;
		for (int j = 0; j < A[0].length; j++) {
			for (int i = 0; i < A.length; i++) {
				sum += A[i][j].abs() * A[i][j].abs();
			}
		}
		return Math.sqrt(sum);
	}
}
