package winterSolutionMethods;

import org.apache.commons.math3.complex.Complex;

import static winterSolutionMethods.ComplexCalc.*;

public class JacobiMethod {

	public static void main(String[] args) {
		Complex[][] A = new Complex[3][3];
		A[0][0] = new Complex(0, 1);
		A[0][1] = new Complex(0, 1);
		A[0][2] = new Complex(0, 0);

		A[1][0] = new Complex(0, 0);
		A[1][1] = new Complex(1, 0);
		A[1][2] = new Complex(0, 0);

		A[2][0] = new Complex(0, 0);
		A[2][1] = new Complex(0, 0);
		A[2][2] = new Complex(1, 1);

		Complex[] b = new Complex[3];
		b[0] = new Complex(1, 0);
		b[1] = new Complex(0, 1);
		b[2] = new Complex(1, 1);

		Complex[] x_0 = new Complex[3];
		for (int i = 0; i < x_0.length; i++) {
			x_0[i] = new Complex(0, 0);
		}
		printVec(jacobiMethod(A, b, x_0));
	}

	public static Complex[] jacobiMethod(Complex[][] A, Complex[] b,
			Complex[] x_mae) {
		final double epsilon = 1.0E-10;
		final int Nmax = 1000;

		Complex[] x = new Complex[x_mae.length];
		for (int i = 0; i < x.length; i++) {
			x[i] = new Complex(0, 0);
		}

		for (int m = 0; m < Nmax; m++) {
			for (int i = 0; i < x.length; i++) {
				Complex sum = new Complex(0, 0);
				for (int j = 0; j < x.length; j++) {
					if (i != j) {
						sum = sum.add(A[i][j].multiply(x_mae[j]));
					}
				}

				x[i] = (b[i].subtract(sum)).divide(A[i][i]);
			}

			// ∞-誤差ノルムによる判定

			// if (vecNormInf(subVec(x, x_mae)) < epsilon) {
			// System.out.printf("判定法は∞-誤差ノルム%n反復回数は%d回%n", m + 1);
			// return x;
			// }

			// ∞-残差ノルムによる判定

			// if (vecNormInf(residual(A, x, b)) < epsilon) {
			// System.out.printf("判定法は∞-残差ノルム%n反復回数は%d回%n", m + 1);
			// return x;
			// }

			// ∞-相対誤差ノルムによる判定

			if (vecNormInf(subVec(x, x_mae)) / vecNormInf(x) < epsilon) {
				System.out.printf("判定法は∞-相対誤差ノルム%n反復回数は%d回%n", m + 1);
				return x;
			}

			// ∞-相対残差ノルムによる判定

			// if (vecNormInf(residual(A, x, b)) / vecNormInf(b) < epsilon) {
			// System.out.printf("判定法は∞-相対残差ノルム%n反復回数は%d回%n", m + 1);
			// return x;
			// }

			for (int i = 0; i < x.length; i++) {
				x_mae[i] = x[i];
			}
		}

		System.out.println("収束しない");
		return null;
	}
}
