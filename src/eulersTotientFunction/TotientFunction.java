package eulersTotientFunction;

import java.math.*;
import java.util.*;

public class TotientFunction {

	// 関数呼び出し部(別クラスに分離可能)
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("n = ");
		BigInteger n = new BigInteger(scan.nextLine());
		scan.close();
		System.out.println("φ(" + n + ") = " + phi(n));
	}

	// 実装部
	public static BigInteger phi(BigInteger n) {
		BigInteger sum = BigInteger.ONE;
		for (BigInteger i = new BigInteger("2"); i.compareTo(n) < 0; i = i.add(BigInteger.ONE)) {
			if (n.gcd(i).equals(BigInteger.ONE)) {
				sum = sum.add(BigInteger.ONE);
			}
		}
		return sum;
	}

}
