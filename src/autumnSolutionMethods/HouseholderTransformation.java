package autumnSolutionMethods;

import static autumnSolutionMethods.Calc.*;

public class HouseholderTransformation {

	public static void main(String[] args) {
		double[][] A = new double[4][4];

		// Hilbert行列の作成
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				A[i][j] = 1.0 / ((i + 1) + (j + 1) - 1);
			}
		}

		printMat(ht(A));
	}

	public static double[][] ht(double[][] A) {
		double[][] a = new double[A.length][A[0].length];
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				a[i][j] = A[i][j];
			}
		}
		int n = a.length;

		double s, gamma;
		double[] se, f, gt;

		for (int k = 0; k < n - 2; k++) {
			double[] u = new double[n];
			for (int i = k + 1; i < n; i++) {
				u[i] = a[i][k];
			}
			if (Math.signum(a[k + 1][k]) == 0.0) {
				s = vecNorm2(u);
			} else {
				s = -Math.signum(a[k + 1][k]) * vecNorm2(u);
			}
			se = new double[n];
			se[k + 1] = s;
			u = scalarDivide(vecNorm2(subVec(u, se)), subVec(u, se));
			f = matVec(a, u);
			gt = vecMat(u, a);
			gamma = innProd(gt, u);
			f = subVec(f, scalarMultiple(gamma, u));
			gt = subVec(gt, scalarMultiple(gamma, u));
			a = addMat(a, addMat(scalarMat(-2, vecProd(u, gt)), scalarMat(-2, vecProd(f, u))));
			printMat(a);
			System.out.println();
		}

		return a;
	}

	public static double[] scalarDivide(double c, double x[]) {
		double[] result = new double[x.length];
		for (int i = 0; i < x.length; i++) {
			result[i] = x[i] / c;
		}
		return result;
	}

	public static double[] vecMat(double x[], double A[][]) {
		double[] result = new double[x.length];
		for (int i = 0; i < x.length; i++) {
			for (int k = 0; k < A.length; k++) {
				result[i] += x[k] * A[k][i];
			}

		}

		return result;
	}

	public static double[][] scalarMat(double c, double A[][]) {
		double[][] result = new double[A.length][A.length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++)
				result[i][j] = c * A[i][j];
		}
		return result;
	}

	public static double[][] vecProd(double x[], double y[]) {
		double[][] result = new double[x.length][x.length];
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result.length; j++)
				result[i][j] = x[i] * y[j];
		}
		return result;
	}

}
