package autumnSolutionMethods;

import static autumnSolutionMethods.Calc.*;

public class QR_仮_ {

	public static void main(String[] args) {
		final int n = 10;

		double[][] a = { { 5.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
				{ 2.0, 5.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 2.0, 5.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 2.0, 5.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 2.0, 5.0, 2.0, 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0, 2.0, 5.0, 2.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0, 0.0, 2.0, 5.0, 2.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 2.0, 5.0, 2.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 2.0, 5.0, 2.0 },
				{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 2.0, 5.0 } };
		double[][] r = new double[n][n];
		double[] b = { 7.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 7.0 };

		householderTransformation(a, r, b);

		double[] x = new double[n];

		/* Rx= Q^T･bをxについて解く(後退代入) */
		x[n - 1] = b[n - 1] / r[n - 1][n - 1];
		for (int i = n - 2; i >= 0; i--) {
			double tmp = 0;
			for (int j = i + 1; j < n; j++) {
				tmp = tmp + r[i][j] * x[j];
			}
			x[i] = (b[i] - tmp) / r[i][i];
		}
		printVec(x);

	}

	public static void householderTransformation(double[][] a, double[][] r, double[] b) {
		/* n次 */
		int n = a.length;

		/* AをRに入れる */
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				r[i][j] = a[i][j];
			}
		}

		/* RとQ^T･bの計算 */
		double[] v = new double[n];
		for (int j = 0; j < n; j++) {
			double tmp = 0;
			for (int i = j + 1; i < n; i++) {
				tmp = tmp + (r[i][j]) * (r[i][j]);
			}
			v[j] = r[j][j] + Math.signum(r[j][j]) * Math.sqrt((r[j][j]) * (r[j][j]) + tmp);
			tmp = Math.sqrt(v[j] * v[j] + tmp);
			for (int i = j + 1; i < n; i++) {
				v[j] = r[j][j] / tmp;
			}
			for (int k = j; k < n; k++) {
				tmp = 0;
				for (int i = j; i < n; i++) {
					tmp = tmp + v[i] + r[i][k];
				}
				for (int i = j; i < n; i++) {
					r[i][k] = r[i][k] - 2 * tmp * v[i];
				}
			}
			tmp = 0;
			for (int i = j; i < n; i++) {
				tmp = tmp + v[i] * b[i];
			}
			for (int i = j; i < n; i++) {
				b[i] = b[i] - 2 * tmp * v[i];
			}

		}

		return;
	}

}
