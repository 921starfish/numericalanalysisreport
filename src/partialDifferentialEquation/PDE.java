package partialDifferentialEquation;

import java.io.*;

public class PDE {
	public static void main(String[] args) throws Exception {
		String crlf = System.getProperty("line.separator");

		int i, j;
		double dt = 0.001;
		double dx = 0.01; // dy = dxとしdyは省略
		double kappa = 0.002347; // 鉄の熱伝達係数 m^2/s
		double lambda = kappa * dt / dx / dx;
		System.out.println(" alpha = " + lambda);
		int height = 61; // 格子点の数 (長さ60cm)
		int width = 41; // 格子点の数 (幅40cm)
		int m = 50000; // 時間格子の数
		double[][] T = new double[height][width];
		double[][] TT = new double[height][width];
		// 初期条件の設定*********************
		for (i = 0; i < height; i++) {
			for (j = 0; j < width; j++) {
				if ((i - 30) * (i - 30) + (j - 20) * (j - 20) <= 10 * 10) {
					T[i][j] = 773.0;
				} else {
					T[i][j] = 300.0;
				}
			}
		}
		int nn = 0;
		FileWriter fw = new FileWriter("PDE.csv"); // ファイル名の指定
		fw.write("t = " + nn + " , ");
		for (j = 0; j < width; j++) {
			fw.write(" " + j + " , ");
		}
		fw.write(crlf);

		// 初期値の格納
		for (i = 0; i < height; i++) {
			fw.write(i + " , ");
			for (j = 0; j < width; j++) {
				fw.write(" " + (T[i][j] - 273.0) + " , ");
			}
			fw.write(crlf);
		}
		fw.write(crlf);

		// 内部温度の計算
		while (nn <= m) {
			nn++;
			for (i = 1; i < (height - 1); i++) {
				for (j = 1; j < (width - 1); j++) {
					TT[i][j] = lambda * (T[i - 1][j] + T[i][j - 1] + T[i + 1][j] + T[i][j + 1])
							+ (1 - 2 * lambda - 2 * lambda) * T[i][j];
				}
			}
			for (i = 1; i < (height - 1); i++) {
				for (j = 1; j < (width - 1); j++) {
					T[i][j] = TT[i][j];
				}
			}
			for (i = 0; i < height; i++) {// 火源はディリクレ条件とする
				for (j = 0; j < width; j++) {
					if ((i - 30) * (i - 30) + (j - 20) * (j - 20) <= 10 * 10) {
						T[i][j] = 773.0;
					}
				}
			}
			for (i = 0; i < height; i++) {// 境界はノイマン条件とする 左右
				T[i][0] = T[i][1];
				T[i][width - 1] = T[i][width - 2];
			}

			for (j = 0; j < width; j++) {// 境界はノイマン条件とする 上下
				T[0][j] = T[1][j];
				T[height - 1][j] = T[height - 2][j];
			}

			// 計算結果の印刷
			if ((nn) % 1000 == 0) {
				fw.write("t = " + nn + " , ");
				for (j = 0; j < width; j++) {
					fw.write(" " + j + " , ");
				}
				fw.write(crlf);
				for (i = 0; i < height; i++) {
					fw.write(i + " , ");
					for (j = 0; j < width; j++) {
						fw.write(" " + (T[i][j] - 273.0) + " , ");
					}
					fw.write(crlf);
				}
				fw.write(crlf);
			}
		}

		fw.flush();
		fw.close();
	}
}