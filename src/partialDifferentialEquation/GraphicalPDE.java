package partialDifferentialEquation;

import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.Alert.*;
import javafx.scene.input.*;
import javafx.stage.*;
import javafx.application.*;
import javafx.beans.value.*;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.text.*;
import javafx.geometry.*;
import javafx.scene.layout.*;

public class GraphicalPDE extends Application {

	static double[][][] log;
	static boolean isDiscrete = true;
	int count;
	double X, Y, W, H;
	GridPane root;
	Pane pane;
	Label second;
	Slider slider;

	public static void main(String[] args) throws Exception {
		int i, j;
		double dt = 0.001;
		double dx = 0.01;
		double kappa = 0.002347; // 鉄の熱伝達係数 m^2/s
		double lambda = kappa * dt / dx / dx;
		System.out.println(" alpha = " + lambda);
		int height = (int) (0.6 / dx) + 1; // 格子点の数 (長さ60cm)
		int width = (int) (0.4 / dx) + 1; // 格子点の数 (幅40cm)
		int m = 50000; // 時間格子の数
		double[][] T = new double[height][width];
		double[][] TT = new double[height][width];
		log = new double[(int) ((m / 1000d) + 1)][height][width];
		// 初期条件の設定*********************
		for (i = 0; i < height; i++) {
			for (j = 0; j < width; j++) {
				if ((i - height / 2) * (i - height / 2) + (j - width / 2) * (j - width / 2) <= (0.1 / dx + 0.5)
						* (0.1 / dx + 0.5)
						&& (i - height / 2) * (i - height / 2) + (j - width / 2) * (j - width / 2) >= (0.1 / dx - 0.5)
								* (0.1 / dx - 0.5)) {
					T[i][j] = 773.0;
				} else {
					T[i][j] = 300.0;
				}
			}
		}

		int nn = 0;
		// 初期値の格納
		for (i = 0; i < height; i++) {
			for (j = 0; j < width; j++) {
				log[nn][i][j] = T[i][j] - 273;
			}
		}
		// 内部温度の計算
		while (nn <= m) {
			nn++;
			for (i = 1; i < (height - 1); i++) {
				for (j = 1; j < (width - 1); j++) {
					TT[i][j] = lambda * (T[i - 1][j] + T[i][j - 1] + T[i + 1][j] + T[i][j + 1])
							+ (1 - 2 * lambda - 2 * lambda) * T[i][j];
				}
			}
			for (i = 1; i < (height - 1); i++) {
				for (j = 1; j < (width - 1); j++) {
					T[i][j] = TT[i][j];
				}
			}
			for (i = 0; i < height; i++) {// 火源はディリクレ条件とする
				for (j = 0; j < width; j++) {
					if ((i - height / 2) * (i - height / 2) + (j - width / 2) * (j - width / 2) <= (0.1 / dx + 0.5)
							* (0.1 / dx + 0.5)
							&& (i - height / 2) * (i - height / 2)
									+ (j - width / 2) * (j - width / 2) >= (0.1 / dx - 0.5) * (0.1 / dx - 0.5)) {
						T[i][j] = 773.0;
					}
				}
			}
			for (i = 0; i < height; i++) {// 境界はノイマン条件とする 左右
				T[i][0] = T[i][1];
				T[i][width - 1] = T[i][width - 2];
			}

			for (j = 0; j < width; j++) {// 境界はノイマン条件とする 上下
				T[0][j] = T[1][j];
				T[height - 1][j] = T[height - 2][j];
			}

			// 計算結果の格納
			if ((nn) % 1000 == 0) {
				for (i = 0; i < height; i++) {
					for (j = 0; j < width; j++) {
						log[(nn) / 1000][i][j] = T[i][j] - 273;
					}
				}
			}
		}

		// GUIの起動
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {

		root = new GridPane();
		root.setAlignment(Pos.CENTER);

		// グラフ本体
		pane = new Pane();
		root.add(pane, 1, 1, 1, 1);

		// カラーバー
		Pane colorBar = new Pane();
		setColorBar(colorBar);
		root.add(colorBar, 3, 1, 1, 1);
		root.setValignment(colorBar, VPos.CENTER);

		// 秒数表示
		second = new Label();
		root.add(second, 1, 3, 1, 1);
		root.setHalignment(second, HPos.CENTER);

		// 初期値の描画
		count = 0;
		setGraph(pane, count);

		// スライダー
		slider = new Slider(0, log.length - 1, 0);
		slider.setShowTickLabels(true);
		slider.setShowTickMarks(true);
		slider.setMajorTickUnit(log.length - 1);
		slider.setMinorTickCount(5);
		slider.setBlockIncrement(1);
		slider.valueProperty().addListener((ObservableValue<? extends Number> observ, Number oldVal, Number newVal) -> {
			count = (int) newVal.doubleValue();
			setGraph(pane, count);
		});
		root.add(slider, 1, 4, 1, 1);

		// +ボタン、-ボタン
		Button plusButton = new Button("+");
		plusButton.setOnAction(event -> {
			if (count < log.length - 1) {
				setGraph(pane, ++count);
				slider.setValue(slider.getValue() + 1);
			}
		});
		Button minusButton = new Button("-");
		minusButton.setOnAction(event -> {
			if (count > 0) {
				setGraph(pane, --count);
				slider.setValue(slider.getValue() - 1);
			}
		});
		root.add(plusButton, 2, 3, 1, 1);
		root.add(minusButton, 0, 3, 1, 1);

		// 連続・離散の切り替えボタン
		Button discreteButton = new Button("↔");
		discreteButton.setOnAction(event -> {
			isDiscrete = !isDiscrete;
			setColorBar(colorBar);
			setGraph(pane, count);
		});
		root.add(discreteButton, 3, 1, 1, 1);
		root.setValignment(discreteButton, VPos.BOTTOM);

		// その他文字ラベル等
		Text title = new Text("グラフ");
		root.add(title, 1, 0, 1, 1);
		root.setHalignment(title, HPos.CENTER);

		root.add(new Label("　 60"), 0, 0, 1, 1);
		root.add(new Label("　　0"), 0, 2, 1, 1);
		root.add(new Label("40　　"), 2, 2, 1, 1);

		Scene s = new Scene(root, 700, 700);
		s.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
		});

		primaryStage.setScene(s);
		primaryStage.setTitle("２次元熱伝導方程式の数値計算");
		primaryStage.show();
	}

	// グラフ描画関数
	public void setGraph(Pane pane, int i) {
		X = 0;
		Y = 0;
		W = 410;
		H = 610;
		
		double h = (H - Y) / log[0].length;
		double w = (W - X) / log[0][0].length;

		pane.getChildren().clear();
		for (int j = 0; j < log[0].length; j++) {
			for (int k = 0; k < log[0][0].length; k++) {
				Rectangle rect = new Rectangle(w, h, makeColor(log[i][j][k]));
				rect.setAccessibleText(String.valueOf(log[i][j][k]));
				rect.setOnMouseClicked(event -> {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle(null);
					alert.getDialogPane().setHeaderText(null);
					alert.getDialogPane().setContentText(rect.getAccessibleText() + "℃");
					alert.show();
				});
				rect.setX(X + w * k);
				rect.setY(Y + h * j);
				rect.setStroke(Color.GRAY);
				pane.getChildren().add(rect);
			}
		}

		second.setText("t=" + i);
	}

	// カラーバー描画関数
	public void setColorBar(Pane pane) {
		for (int i = 0; i <= 500; i++) {
			Rectangle rect = new Rectangle(50, 1, makeColor(500 - i));
			rect.setX(0);
			rect.setY(i);
			Tooltip t = new Tooltip(String.valueOf(500 - i));
			Tooltip.install(rect, t);
			pane.getChildren().add(rect);
		}
	}

	// 温度を色に変える関数
	public Color makeColor(double t) {
		if (!isDiscrete) {
			return Color.hsb((1 - t / 500) * 300, 1.0, 1.0);
		}

		if (0 < t && t <= 50) {
			return Color.hsb((1 - 27.0 / 500) * 300, 1.0, 1.0);
		} else if (50 < t && t <= 100) {
			return Color.BLUEVIOLET;
		} else if (100 < t && t <= 150) {
			return Color.BLUE;
		} else if (150 < t && t <= 200) {
			return Color.CYAN;
		} else if (200 < t && t <= 250) {
			return Color.LIME;
		} else if (250 < t && t <= 300) {
			return Color.LAWNGREEN;
		} else if (300 < t && t <= 350) {
			return Color.GREENYELLOW;
		} else if (350 < t && t <= 400) {
			return Color.YELLOW;
		} else if (400 < t && t <= 450) {
			return Color.ORANGE;
		} else if (450 < t && t <= 500) {
			return Color.RED;
		} else {
			return Color.TRANSPARENT;
		}
	}
}