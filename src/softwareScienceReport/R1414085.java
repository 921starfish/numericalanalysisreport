package softwareScienceReport;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class R1414085 {
	/**
	 * data[i][]が受験番号i+1番の受験生のデータの配列
	 * それぞれの配列には
	 *  [受験番号, 数学IIB, 数学IIIB, 英語, 物理B, 化学B, 生物B, 地学B, 合計点, 順位データ]
	 * を格納する
	 */
	static int[][] data;
	/**
	 * rika[i]が受験番号i+1番の受験生の理科採用科目
	 */
	static String[] rika;

	public static void main(String[] args) {
		ArrayList<String[]> list = new ArrayList<String[]>();

		// ファイルの読み込み ここから
		System.out.println("Seiseki.datのファイルパスを入力してください");
		System.out.print("path = ");
		Scanner scan = new Scanner(System.in);
		String path = scan.nextLine();
		scan.close();
		BufferedReader br = null;
		try {
			File file = new File(path);
			br = new BufferedReader(new FileReader(file));
			Pattern p = Pattern.compile("[\\s]+");
			String str = br.readLine();
			while (str != null) {
				String[] s = p.split(str);
				list.add(s);
				str = br.readLine();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		data = new int[list.size()][10];
		rika = new String[list.size()];
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < list.get(i).length - 1; j++) {
				data[i][j] = Integer.valueOf(list.get(i)[j + 1]);
			}
		}
		// ファイルの読み込み ここまで

		// 理科の上位２科目を求める ここから
		for (int i = 0; i < data.length; i++) {
			if (data[i][4] >= data[i][5] && data[i][4] >= data[i][6] && data[i][4] >= data[i][7]) {
				if (data[i][5] >= data[i][6]) {
					if (data[i][5] >= data[i][7]) {
						rika[i] = "物理,化学";
					}
				}
				if (data[i][6] >= data[i][7]) {
					rika[i] = "物理,生物";
				} else {
					rika[i] = "物理,地学";
				}
			} else if (data[i][5] >= data[i][4] && data[i][5] >= data[i][6] && data[i][5] >= data[i][7]) {
				if (data[i][4] >= data[i][6]) {
					if (data[i][4] >= data[i][7]) {
						rika[i] = "化学,物理";
					}
				}
				if (data[i][6] >= data[i][7]) {
					rika[i] = "化学,生物";
				} else {
					rika[i] = "化学,地学";
				}
			} else if (data[i][6] >= data[i][4] && data[i][6] >= data[i][5] && data[i][6] >= data[i][7]) {
				if (data[i][4] >= data[i][5]) {
					if (data[i][4] >= data[i][7]) {
						rika[i] = "生物,物理";
					}
				}
				if (data[i][5] >= data[i][7]) {
					rika[i] = "生物,化学";
				} else {
					rika[i] = "生物,地学";
				}
			} else if (data[i][7] >= data[i][4] && data[i][7] >= data[i][5] && data[i][7] >= data[i][6]) {
				if (data[i][4] >= data[i][5]) {
					if (data[i][4] >= data[i][6]) {
						rika[i] = "地学,物理";
					}
				}
				if (data[i][5] >= data[i][6]) {
					rika[i] = "地学,化学";
				} else {
					rika[i] = "地学,生物";
				}
			}
		}
		// 理科の上位２科目を求める ここまで

		// 理科を点数順にソート
		for (int i = 0; i < data.length; i++) {
			for (int j = 4; j < 7; j++) {
				for (int k = 7; k > j; k--) {
					if (data[i][k] > data[i][k - 1]) {
						int t = data[i][k];
						data[i][k] = data[i][k - 1];
						data[i][k - 1] = t;
					}
				}
			}
		}

		// 順位判定に使うために合計を出す
		for (int i = 0; i < data.length; i++) {
			for (int j = 1; j <= 5; j++) {
				data[i][8] += data[i][j];
			}
		}

		// 求めた合計を使ってソート
		for (int h = 0; h < data.length - 1; h++) {
			for (int i = data.length - 1; i > h; i--) {
				if (data[i][8] > data[i - 1][8]) {
					int[] t = data[i];
					data[i] = data[i - 1];
					data[i - 1] = t;

					String s = rika[i];
					rika[i] = rika[i - 1];
					rika[i - 1] = s;
				}
			}
		}

		// 順位を入れる
		int rank = 1;
		int count = 1;
		data[0][9] = rank;
		for (int i = 1; i < data.length; i++) {
			if (data[i][8] == data[i - 1][8]) {
				data[i][9] = rank;
				count++;
			} else {
				rank += count;
				data[i][9] = rank;
				count = 1;
			}
		}

		for (int i = 0; i < data.length; i++) {
			if (data[i][9] > 300) { // 順位が301位以上の時は出力しない
				break;
			}
			System.out.printf("順位 %3d位 受験番号 %4d番 得点 %3d点 ", data[i][9], data[i][0], data[i][8]);
			System.out.println(rika[i]);
		}
	}
}
