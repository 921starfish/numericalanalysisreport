// 真の解は毎回書き換える
package summerSolutionMethods;

import org.apache.commons.math3.complex.*;

public class SecantMethod {

	public static void secantMethod(Complex x0, double epsilon, int Nmax,
			ComplexUnaryOperator f, ComplexUnaryOperator df) {
		System.out.println("セカント法");
		System.out.println("初期値：x0 = " + x0);
		final Complex x1 = new Complex(x0.getReal() + 1.0, x0.getImaginary());
		System.out.println("初期値：x1 = " + x1);
		int k;

		// 相対誤差による収束判定
		Complex x = new Complex(x0.getReal(), x0.getImaginary());
		Complex x_bb = new Complex(x0.getReal(), x0.getImaginary());
		Complex x_b = new Complex(x1.getReal(), x1.getImaginary());
		for (k = 0; k < Nmax; k++) {
			Complex temp = new Complex(x.getReal(), x.getImaginary());
			x = x.subtract(f.applyAsComplex(x).multiply(
					(x_b.subtract(x_bb)).divide(f.applyAsComplex(x_b).subtract(
							f.applyAsComplex(x_bb)))));
			x_bb = new Complex(x_b.getReal(), x_b.getImaginary());
			x_b = new Complex(x.getReal(), x.getImaginary());
			if (((x.subtract(temp)).abs()) / x.abs() < epsilon) {
				break;
			}
		}
		System.out.print("相対誤差による収束判定の");
		System.out.println("近似解は x" + (k + 1) + "=" + x);
		System.out.print("このとき絶対誤差は");
		System.out.println(x.subtract(new Complex(1.0, 0.0)).abs());

		// 誤差による収束判定
		x = new Complex(x0.getReal(), x0.getImaginary());
		x_bb = new Complex(x0.getReal(), x0.getImaginary());
		x_b = new Complex(x1.getReal(), x1.getImaginary());
		for (k = 0; k < Nmax; k++) {
			Complex temp = new Complex(x.getReal(), x.getImaginary());
			x = x.subtract(f.applyAsComplex(x).multiply(
					(x_b.subtract(x_bb)).divide(f.applyAsComplex(x_b).subtract(
							f.applyAsComplex(x_bb)))));
			x_bb = new Complex(x_b.getReal(), x_b.getImaginary());
			x_b = new Complex(x.getReal(), x.getImaginary());
			if (((x.subtract(temp))).abs() < epsilon) {
				break;
			}
		}
		System.out.print("誤差による収束判定の");
		System.out.println("近似解は x" + (k + 1) + "=" + x);
		System.out.print("このとき絶対誤差は");
		System.out.println(x.subtract(new Complex(1.0, 0.0)).abs());

		// 残差による収束判定
		x = new Complex(x0.getReal(), x0.getImaginary());
		x_bb = new Complex(x0.getReal(), x0.getImaginary());
		x_b = new Complex(x1.getReal(), x1.getImaginary());
		for (k = 0; k < Nmax; k++) {
			if (f.applyAsComplex(x).abs() < epsilon) {
				break;
			}
			x = x.subtract(f.applyAsComplex(x).multiply(
					(x_b.subtract(x_bb)).divide(f.applyAsComplex(x_b).subtract(
							f.applyAsComplex(x_bb)))));
			x_bb = new Complex(x_b.getReal(), x_b.getImaginary());
			x_b = new Complex(x.getReal(), x.getImaginary());
		}
		System.out.print("残差による収束判定の");
		System.out.println("近似解は x" + k + "=" + x);
		System.out.print("このとき絶対誤差は");
		System.out.println(x.subtract(new Complex(1.0, 0.0)).abs());
	}
}
