package summerSolutionMethods;

import org.apache.commons.math3.complex.Complex;

public class Main {

	public static void main(String[] args) {
		Complex x0 = new Complex(2.0, 0.0);
		double epsilon = 1E-10;
		int Nmax = 1000;
		ComplexUnaryOperator f = x -> x.multiply(x).multiply(x).subtract(1);
		ComplexUnaryOperator df = x -> x.multiply(x).multiply(3);

		NewtonMethod.newtonMethod(x0, epsilon, Nmax, f, df);

		System.out.println();

		SecantMethod.secantMethod(x0, epsilon, Nmax, f, df);

		System.out.println();

		ParallelChordMethod.parallelChordMethod(x0, epsilon, Nmax, f, df);
	}

}
