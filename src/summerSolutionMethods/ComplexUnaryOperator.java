package summerSolutionMethods;

import java.util.Objects;
import org.apache.commons.math3.complex.*;

@FunctionalInterface
public interface ComplexUnaryOperator {

	public abstract Complex applyAsComplex(Complex operand);

	default ComplexUnaryOperator compose(ComplexUnaryOperator before) {
		Objects.requireNonNull(before);
		return (Complex v) -> {
			return applyAsComplex(before.applyAsComplex(v));
		};
	}

	default ComplexUnaryOperator andThen(ComplexUnaryOperator after) {
		Objects.requireNonNull(after);
		return (Complex t) -> {
			return after.applyAsComplex(applyAsComplex(t));
		};
	}

	static ComplexUnaryOperator identity() {
		return (Complex t) -> {
			return t;
		};
	}
}
