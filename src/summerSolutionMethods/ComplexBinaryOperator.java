package summerSolutionMethods;

import org.apache.commons.math3.complex.*;

@FunctionalInterface
public interface ComplexBinaryOperator {
	
	public abstract Complex applyAsComplex(Complex left, Complex right);
	
}
