package springSolutionMethods;

import static springSolutionMethods.Calc.*;

public class CGMethod {
	public static void cGMethod(double[][] a, double[] x_0, double[] b, double epsilon, int Nmax) {
		final int N = x_0.length;
		// 引数のx_0[]を書き換えないために新たにx[]を置く
		double[] x = new double[N];
		for (int i = 0; i < N; i++) {
			x[i] = x_0[i];
		}
		double[] p = new double[N]; // 方向(探索)ベクトル
		double[] r = new double[N]; // 残差ベクトル
		double[] ax = matVec(a, x);
		double[] ap = new double[N];
		double alpha = 0;
		double beta = 0;

		// p^(0) = r^(0) = b - Ax^(0)を求める
		for (int i = 0; i < N; i++) {
			p[i] = b[i] - ax[i];
			r[i] = p[i];
		}
		// 反復計算
		for (int k = 0; k < Nmax; k++) {
			// alphaを計算
			ap = matVec(a, p);
			alpha = innProd(p, r) / innProd(p, ap);

			// xとrの更新
			for (int i = 0; i < N; i++) {
				x[i] = x[i] + alpha * p[i];
				r[i] = r[i] - alpha * ap[i];
			}

			// 反復回数や誤差の表示
			System.out.printf("%d回目の反復\t", k);
			System.out.println("相対残差は" + vecNorm2(r) / vecNorm2(b));

			// 収束判定
			if (vecNorm2(r) <= epsilon * vecNorm2(b)) {
				System.out.println(k + "回目で収束したので終了");
				printVec(x);
				break;
			}

			// 収束しなかったので、βとpの更新
			beta = -innProd(ap, r) / innProd(ap, p);
			for (int i = 0; i < N; i++) {
				p[i] = r[i] + beta * p[i];
			}
		}
	}
}
