package springSolutionMethods;

import static springSolutionMethods.Calc.*;

public class ImprovedCGMethod {
	public static void improvedCGMethod(double[][] a, double[] x_0, double[] b, double epsilon, int Nmax) {
		final int N = x_0.length;
		// 引数のx_0[]を書き換えないために新たにx[]を置く
		double[] x = new double[N];
		for (int i = 0; i < N; i++) {
			x[i] = x_0[i];
		}
		double[] p = new double[N]; // 方向(探索)ベクトル
		double[] r = new double[N]; // 残差ベクトル
		double[] ax = matVec(a, x);
		double[] ap = new double[N];
		double alpha = 0;
		double beta = 0;
		double r_k_no_2jou = 0;

		// p^(0) = r^(0) = b - Ax^(0)を求める
		for (int i = 0; i < N; i++) {
			p[i] = b[i] - ax[i];
			r[i] = p[i];
		}
		// 反復計算
		for (int k = 0; k < Nmax; k++) {
			// alphaを計算
			ap = matVec(a, p);
			r_k_no_2jou = vecNorm2_no_2jou(r); // r_kの値を保持
			alpha = r_k_no_2jou / innProd(p, ap);

			// xとrの更新
			for (int i = 0; i < N; i++) {
				x[i] = x[i] + alpha * p[i];
				r[i] = r[i] - alpha * ap[i];
			}

			// 反復回数や誤差の表示
			System.out.printf("%d回目の反復\t", k);
			System.out.println("相対残差は" + vecNorm2(r) / vecNorm2(b));

			// 収束判定
			if (vecNorm2(r) <= epsilon * vecNorm2(b)) {
				System.out.println(k + "回目で収束したので終了");
				printVec(x);
				break;
			}

			// 収束しなかったので、βとpの更新
			beta = vecNorm2_no_2jou(r) / r_k_no_2jou; // r_k+1はメソッドから、r_kはさっき保持した値
			for (int i = 0; i < N; i++) {
				p[i] = r[i] + beta * p[i];
			}
		}
	}

	public static double vecNorm2_no_2jou(double x[]) {
		double result = 0;
		for (int i = 0; i < x.length; i++) {
			result += x[i] * x[i];
		}
		return result;
	}
}
