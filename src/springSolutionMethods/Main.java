package springSolutionMethods;

import java.util.Random;

public class Main {

	public static void main(String[] args) {
		double[][] a = new double[10][10];

		// Hilbert行列の作成
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				a[i][j] = 1.0 / ((i + 1) + (j + 1) - 1);
			}
		}

		a[0][0] *= -1;a[0][2] *= -1;a[0][7] *= -1;a[0][8] *= -1;a[0][9] *= -1;
		a[1][4] *= -1;a[1][5] *= -1;a[1][8] *= -1;
		a[2][0] *= -1;a[2][2] *= -1;a[2][6] *= -1;a[2][8] *= -1;
		a[3][3] *= -1;a[3][7] *= -1;a[3][9] *= -1;
		a[4][1] *= -1;a[4][4] *= -1;a[4][5] *= -1;a[4][6] *= -1;a[4][8] *= -1;a[4][9] *= -1;
		a[5][1] *= -1;a[5][4] *= -1;a[5][6] *= -1;
		a[6][2] *= -1;a[6][4] *= -1;a[6][5] *= -1;a[6][8] *= -1;a[6][9] *= -1;
		a[7][0] *= -1;a[7][3] *= -1;a[7][9] *= -1;
		a[8][0] *= -1;a[8][1] *= -1;a[8][2] *= -1;a[8][4] *= -1;a[8][6] *= -1;
		a[9][0] *= -1;a[9][3] *= -1;a[9][4] *= -1;a[9][6] *= -1;a[9][7] *= -1;
		Calc.printMat(a);
		
		// bの調整
		double[] b = new double[10];
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				b[i] += a[i][j];
			}
		}
		double[] x = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		CGMethod.cGMethod(a, x, b, 1.0E-10, 1000);
		ImprovedCGMethod.improvedCGMethod(a, x, b, 1.0E-10, 1000);

	}

}
